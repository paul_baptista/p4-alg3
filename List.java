/**
* Interfaz que define el comportamiento de una lista
* 
* Esta es una clase parametrizada con tipo (clase) E; i.e., la
* lista contiene elementos de tipo E.
*/
public interface List<E> {

     /**
     * Agrega un elemento a la lista.
     */
public boolean add(E element);

     /**
     * Elimina todos los elementos de la lista. La lista queda
     * como recien creada.
     */
public void clear();

     /**
     * Determina si el elemento dado esta en la lista.
     */
public boolean contains(Object element);

     /**
     * Determina si la lista dada o es igual a la lista this.
     */
public boolean equals(Object o);

     /**
     * Retorna una nueva lista que es copia de this.
     */
public List<E> clone();

     /**
     * Determina si la lista es vacia.
     */
public boolean isEmpty();

   /**
    * Retorna el elemento en la posicion pos,
    * 0 <= pos < this.getSize()
    */
public E get(int pos);

   /**
    * Devuelve la instancia de la copia de element almacenada en la lista.
    */
public E get(E element);

     /**
     * Elimina el elemento que esta en la posicion pos de la lista. Si
     * la lista cambia, retorna true, sino retorna false.
     */
public boolean remove(int pos);

     /**
     * Elimina el elemento dado de la lista. Si la lista cambia,
     * retorna true, sino retorna false.
     */
public boolean remove(E element);

     /**
     * Retorna el numero de elementos en la lista
     */
public int getSize();

   /**
    * Concatena this con nextList
    */
public boolean Concat(List<E> nextList);

   /**
    * Retorna una lista producto de la concatenación de this
    * y nextlist
    */
public List<E> RetConcat(List<E> nextList);

     /**
     * Devuelve un iterador sobre la lista.
     */
public ListIterator<E> iterator();


}

// End List.
