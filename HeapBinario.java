/**
 * HeapBinario.java
 * Clase que implementa el manejo de archivos.
 * 
 * Autores: Fabio Castro   10-10132 
 *          Paul Baptista  10-10056
 * Grupo: 09
 * Fecha: 15/12/2013
 */
public class HeapBinario<T extends Comparable> {

	private int tam; // 
	private Comparable[] elementos;



	// Constructor de la clase
	public HeapBinario(int tamInicial) {
		tam = 0;
		//arreglo = new Comparable[tamDefault];
		elementos = new Comparable[tamInicial];
	}


	// Retorna un booleano indicando si el heap esta vacio o no
	public boolean vacia() {
		return tam == 0;
	}


	// Inserta en el heap un elemento elem
	@SuppressWarnings("unchecked")
	public void agrega(Comparable dato) {
		Comparable elem = dato;
		int cantidad; //Espacio donde insertaremos el elemento

		if (tam + 1 == elementos.length)
			redimen();
		// LLeva el nodo a una posicion superior
		cantidad = ++tam;
		elementos[0] = dato;
		for ( ; dato.compareTo(elementos[cantidad / 2]) < 0; cantidad /=2){
			elementos[cantidad]=elementos[cantidad / 2];
		}
		elementos[cantidad]=dato;
	} 


	// Elimina la posicion con el elemento mas pequeno, es decir la primera
	public Comparable extrae() {
        
        Comparable minElem = -1;

        if (!(vacia())) {
			minElem = elementos[1];
        	elementos[1] = elementos[tam--];
        	desplazar(1);
    	}
        return minElem;
    }


    // Desplaza el nodo hacia abajo en el arbol
    @SuppressWarnings("unchecked")
    private void desplazar(int cantidad) {
        
        int nodo_hijo;
        Comparable temp = elementos[cantidad];

        for( ; cantidad * 2 <= tam; cantidad = nodo_hijo ) {
            nodo_hijo = cantidad * 2;
            
            if (nodo_hijo != tam &&
                    elementos[nodo_hijo + 1].compareTo( elementos[nodo_hijo] ) < 0 )
                nodo_hijo++;
            
            if (elementos[nodo_hijo].compareTo(temp) < 0 ){
                elementos[cantidad]= elementos[nodo_hijo];
                }
            else
                break;
        }
        elementos[cantidad] = temp;        
    }


	// En caso de ser necesario el arreglo se redimenciona al doble de su tamaño
	private void redimen() {
    
        Comparable[] nuevoelementos = new Comparable[ elementos.length * 2 ];

        for( int i = 0; i < elementos.length; i++ ){
            nuevoelementos[i] = elementos[i];
        }
        elementos = nuevoelementos;
    }


    /* Retorna el tamao del heap */
	public int getTam() {
		return tam;
	}
	

}