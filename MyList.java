/**
 * Clase que implementa la interfaz List
 * Esta es una clase parametrizada con tipo (clase) E; i.e., la
 * lista contiene elementos de tipo E.
 * @author Paul Baptista (10-10056)
 * @author Fabio Castro (10-10056)
 */
public class MyList<E> implements List<E>{
   
   
   /*
    * Modelo de representacion: lista enlazada.
    *
    */
   private MyListNode head;
   private MyListNode tail;
   private int size;

   /**
    * Clase que implementa nodos de una lista doblemente enlazada
    *
    */
   private class MyListNode{
      
      public E data;
      public MyListNode prev;
      public MyListNode next;
      
      public MyListNode(E d){
         data = d;
         prev = null;
         next = null;
      }
   }
   
   
   /**
    * Constructor
    */
   public MyList() {
      head = null;
      tail = null;
      size = 0;
   }
   
   /**
    * Agrega un elemento al final de la lista.
    */
   public boolean add(E element) {
      if (this.isEmpty()) {
         head = new MyListNode(element);
         tail = head;
      }
      else{
         tail.next = new MyListNode(element);
         tail.next.prev = tail;
         tail = tail.next;
      };
      
      if (tail.data==element){
         size++;
         return true;
      }

      return false;
   }
   

   /**
    * Elimina todos los elementos de la lista. La lista queda
    * como recien creada.
    */
   public void clear(){
      head = null;
      tail = null;
      size = 0;
   }
   
   /**
    * Determina si el elemento dado esta en la lista.
    */
   @SuppressWarnings("unchecked")
   public boolean contains(Object element){
      ListIterator<E> iter = iterator();
      
      while (iter.hasNext()) {
         if (iter.next().equals((E) element))
            return true;
      }
      
      return false;
   }
   
   /**
    * Determina si la lista dada es igual a la lista.
    */
   public boolean equals(Object o){
      List list;
      boolean equal;

      if (!(o instanceof List))
         return false;

      list = (List) o;

      ListIterator iter1 = iterator();
      ListIterator iter2 = list.iterator();
      
      equal = (size == list.getSize());

      while (iter1.hasNext() && equal) {
            equal = equal && iter2.hasNext();
            equal = equal && iter1.next().equals(iter2.next());
      }

      return equal;
   }
    
  /**
   * Retorna una nueva lista que es copia de this.
   */
   public List<E> clone(){

      List<E> newList = new MyList<E>();

      ListIterator<E> iter = iterator();

      while (iter.hasNext()){
         newList.add(iter.next());
      }
      return newList;
   }
   
   /**
    * Determina si la lista es vacia.
    */
   public boolean isEmpty(){
      return size == 0;
   }
   
   /**
    * Retorna el elemento en la posicion pos,
    * 0 <= pos < this.getSize()
    */
   public E get(int pos){
      ListIterator<E> iter = iterator();
      E elem = null;

      if ((pos>getSize())||(pos<=0)) return null;

      while ((pos>0)&&iter.hasNext()) {
         elem = iter.next();
         pos--;
      }

      return elem;
   }

   /**
    * Devuelve la instancia de la copia de element almacenada en la lista.
    */
   public E get(E element){
      E tmp;
      ListIterator<E> iter = iterator();
      
      while (iter.hasNext()) {
         tmp = iter.next();
         if (tmp.equals(element))
            return tmp;
      }
      
      return null;
   }
   
   /**
    * Elimina el elemento que esta en la posicion pos de la lista. Si
    * la lista cambia, retorna true, sino retorna false.  
    *
    * Utilizar esta operacion puede hacer invalido los iteradores
    * sobre this
    */
   public boolean remove(int pos){

      ListIterator iter =  iterator();

      if (pos<=0)
         return false;

      for (int i=0;i<pos;i++) {
         if (!iter.hasNext())
            return false;
         iter.next();
      }

      iter.unlink();
      return true;

   }
   
   /**
    * Elimina el elemento dado de la lista. Si la lista cambia,
    * retorna true, sino retorna false.
    */
   public boolean remove(E element){

      ListIterator iter =  iterator();

      while (iter.hasNext()){
         if (iter.next().equals(element)){
            iter.unlink();
            return true;
         }
      }
      
      return false;
   }
   
   /**
    * Retorna el numero de elementos en la lista
    */
   public int getSize(){
      return size;
   }

   /**
    * Concatena this con nextList
    */
   public boolean Concat(List<E> nList){

      boolean state = true;
      ListIterator<E> iter = nList.iterator();

      while ((iter.hasNext())&&state){
         state = state && add(iter.next());
      }

      return state;
   }

   /**
    * Retorna una lista producto de la concatenación de this
    * y nextlist
    */
   public List<E> RetConcat(List<E> nextList){

      List<E> newList = clone();

      ListIterator<E> nextIter = nextList.iterator();

      while (nextIter.hasNext()){
         newList.add(nextIter.next());
      }

      return newList;
   }
   
   /**
    * Devuelve un iterador sobre la lista.
    */
   public ListIterator<E> iterator() {
	  return new MyListIterator();
   }
   
   /**
   * Clase que implementa la interfaz ListIterator<E>
   */
   public class MyListIterator implements ListIterator<E>{
      private int pos = 0;
      private MyListNode auxNode=null;

      public void reset(){
         pos=0;
      }

      /**
       * Comprueba que exista un proximo elemento.
       */
      public boolean hasNext(){
         return (pos < size);
      }
      
      /**
       * Devuelve el elemento asociado y avanza el iterador.
       */
      public E next(){
         pos++;

         if ( pos == 1){
            auxNode = head;
         }
         else auxNode = auxNode.next;

         return auxNode.data;
      }
      
      /**
       * Remueve de la lista el ultimo elemento retornado por next()
       */
      public void unlink(){
         if (pos>0){

            if (pos==size){

               if (size==1){
                  clear();
                  pos=0;
               }

               else{
                  tail=tail.prev;
                  tail.next=null;
                  auxNode=tail;
                  pos=--size;
               }

            }
            else if (pos<size){

               if (pos==1){
                  head=head.next;
                  head.prev=auxNode=null;
               }
               else{
                  auxNode.prev.next = auxNode.next;
                  auxNode.next.prev = auxNode.prev;
                  auxNode = auxNode.prev;
               }
               pos--;
               size--;

            }
         }
      }
   }
}

// End List.