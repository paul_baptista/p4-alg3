/**
 * BellmanFord.java
 * Clase que representa la ejecución del recorrido de un grafo, dado un nodo
 * origen mediante una modificación del algoritmo de Bellman-Ford para nuestro
 * problema.
 * 
 * Autores: Fabio Castro   10-10132 
 *          Paul Baptista  10-10056
 * Grupo: 09
 * Fecha: 14/12/2014
 */

public class BellmanFord {
	
	/*
    * Grafo a recorrer y nodo origen del recorrido.
    */
   private DigraphHash grafo;
   private Node origen;


   /*
    * Arreglos usados para: 
    *    distancia[i]:  Almacenar las distancias de cada nodo "i" del grafo 
    *                   respecto al origen.
    *    pred[i]: Almacenar los nodos previos a "i" en la representación de los
    *             caminos provenientes del origen a cada nodo.
    */
	private int distancia[ ];
   private Node pred[ ];


   /*
    * Constructor. Recibe el Digraph a ser recorrido y el Node del cual se va a
    * comenzar el mismo.
    *
    * Hace las asignaciones debidas y procede a hacer el recorrido
    * correspondiente.
    */
	public BellmanFord(DigraphHash e, Node origen){
		grafo = e;
      this.origen = origen;
		distancia = new int[grafo.getNumVertices()+1];
      pred = new Node[grafo.getNumVertices()+1];
      init();
      recorrer();
	}


   /*
    * Devuelve la distancia del nodo "origen" al nodo "destino" recibido como
    * parámetro.
    *
    * Se encarga de verificar la inexistencia de ciclos negativos antes de
    * retornar la distancia previamente calculada. En caso de que esta condición
    * no se cumpla retorna Integer.MIN_VALUE.
    */
   public int getDistancia(Node destino){
      Node actual, anterior;
      actual = destino;

      while ( pred[Integer.parseInt(""+actual)]!=null ){

         anterior = pred[Integer.parseInt(""+actual)];

         if (  ( distancia[Integer.parseInt(""+actual)] 
                  != (distancia[Integer.parseInt(""+anterior)]
                                             +actual.getPeso()) )
            )
         {
            return Integer.MIN_VALUE;
         }
         actual = anterior;
      }
      return distancia[Integer.parseInt(""+destino)];
   }


	/*
    * Función que se encarga de hacer la inicialización de los arreglos,
    * necesarias antes de proceder al recorrido.
    *
    * Le da el valor de Integer.MAX_VALUE a las distancias entre cada nodo y el
    * origen, pues no hay caminos calculados.
    */
	private void init(){
	    for( int i = 0 ; i <= grafo.getNumVertices() ; ++i ){
	        distancia[i] = Integer.MAX_VALUE;
	    }

	}


   /*
    * Función encargada de hacer el recorrido.
    *
    * Recorre la lista de los lados a cuyos nodos origen ya se les haya  
    * calculado algún camino y calcula el peso de los nodos destino. Acción que
    * se itera tantas veces como nodos tiene el grafo o ya no se actualice el
    * camino o peso de ningún nodo.
    */
   private void recorrer(){

      Edge lado_actual;
      Node nodo_src, nodo_dst;

      List<Edge> lista_lados;
      ListIterator<Edge> iter_lados;

      lista_lados = grafo.getOutEdges(""+origen);
      iter_lados = lista_lados.iterator();

      distancia[ Integer.parseInt(origen.toString()) ] = origen.getPeso();
      

      boolean relajar = true;
      boolean continuar = true;

      for ( int i=1; (i<grafo.getNumVertices()) && relajar; i++ ){

         relajar = false;
         iter_lados = lista_lados.iterator();

         while ( iter_lados.hasNext() ){

            lado_actual = iter_lados.next();
            nodo_src = grafo.get(lado_actual.getSrc());
            nodo_dst = grafo.get(lado_actual.getDst());
            
            if (  ( distancia[Integer.parseInt(""+nodo_dst)] 
                  > (distancia[Integer.parseInt(""+nodo_src)]
                                                +nodo_dst.getPeso()) )
                  &&
                  ( pred[Integer.parseInt(""+nodo_src)] != nodo_dst )
               )
            {

               if ( distancia[Integer.parseInt(""+nodo_dst)]
                                                == Integer.MAX_VALUE )
               {
                  lista_lados.Concat(grafo.getOutEdges(""+nodo_dst));
               }

               distancia[Integer.parseInt(""+nodo_dst)]
               =
                  (distancia[Integer.parseInt(""+nodo_src)]
                   +nodo_dst.getPeso());

               pred[Integer.parseInt(""+nodo_dst)] = nodo_src;

               relajar = true;
            }
         }

      } 
   }
	
}