/*
 * Archivo: Main.java 
 * Descripcion: Archivo de prueba. Prueba todas las
 * funcionalidades implementadas en forma ordenada y clara.
 *
 * Autores: Fabio Castro   10-10132 
 *          Paul Baptista  10-10056
 * Grupo: 09
 * Fecha: 14/12/2014
 */
import java.io.*; 

public class Main {


   /*
    * Funcion println, imprime en pantalla la representación en String de los
    *   objetos que recibe como parametros
    */
   public static void print(Object... args) {
      for(Object pts: args)
         System.out.print(pts);
   }

   /*
    * Funcion println, imprime en pantalla la representación en String de los
    *   objetos que recibe como parametros
    */
   public static void println(Object... args) {
      for(Object pts: args)
         System.out.print(pts);
      System.out.println();
   }

   /*
    * Funcion ClearScreen imprime 25 saltos de linea y limpiando el terminal
    */

   public static void ClearScreen() {
      int i;
      for(i=0;i<=25;i++){
         println();
      }

   }
   /*
    * Funcion printlist, imprime en pantalla la representación en String de los
    *   objetos que contiene la lista pasada como parametro
    */
   public static void printlist(List lista) {
      ListIterator iter = lista.iterator();
      while (iter.hasNext()) {
         println(iter.next());      

      }
   }


   /*
    * Main Principal que ejecuta todas las pruebas necesarias
    */
   public static void main(String args[]) throws Exception{

      
      int distancia;
      int cantidad;

      if (args.length!=2){
         println("Uso: java Main <archivo.in> <archivo.out>");
         return;
      }

      Archivo lectura_archivo = new Archivo();
      
      DigraphHash grafo;
      BellmanFord recorrido;
            // Archivos de entrada y salida.
      String in = args[0];
      String out = args[1];

      lectura_archivo.openFile(in,out);

     /* Ciclo en el que se hace la lectura de cada uno de los calabozos
      * presentes en el archivo de entrada y escribe los resultados obtenidos en
      * el archivo correspondiente.
      */
      do{     
         grafo = lectura_archivo.readFile();
         if (grafo!=null){
          recorrido = new BellmanFord(grafo,lectura_archivo.getNodoInicial());
          cantidad = recorrido.getDistancia(lectura_archivo.getNodoFinal());
          if (cantidad==Integer.MIN_VALUE)
            lectura_archivo.writeFile("-INF");
          else
            lectura_archivo.writeFile(""+cantidad);
         }
      } while(grafo != null); 

      lectura_archivo.closeFile();
   }

}
