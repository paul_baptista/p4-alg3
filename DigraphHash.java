/**
* Clase concreta de DiGraph
* Uds debe seleccionar elmodelo de representacion que mejor
* satisfaga las restriciones establecidas
*
* Sustituya Hash por la palabra que mejor represente el modelo
* seleccionado
*/

public class DigraphHash extends Digraph{

  /*
  * Modelo de representacion: Hash con manejo de Colisiones.
  *
  */
   private MiTablaHash<Node> Nodos;
   private MiTablaHash<Edge> VerticesDst;
   private MiTablaHash<Edge> VerticesSrc;
 
  /**
  * Construye un grafo vacio, las tablas de hash que componen al mismo son de
  * tamaño 300
  */
  public DigraphHash() {
    int t = 300;
    Nodos = new MiTablaHash<Node>(t);
    VerticesDst = new MiTablaHash<Edge>(t);
    VerticesSrc = new MiTablaHash<Edge>(t);
  }

  /**
  * Construye un grafo vacio, las tablas de hash que componen al mismo son de
  * tamaño t
  */
  public DigraphHash(int t) {
    Nodos = new MiTablaHash<Node>(t);
    VerticesDst = new MiTablaHash<Edge>(t);
    VerticesSrc = new MiTablaHash<Edge>(t);
  }

  /**
  * Construye un grafo vacio, la tabla de hash que corresponde a los nodos del
  * grafoes de tamaño v y la de los nodos de tamaño e
  */
  public DigraphHash(int v, int e) {
    Nodos = new MiTablaHash<Node>(v);
    VerticesDst = new MiTablaHash<Edge>(e);
    VerticesSrc = new MiTablaHash<Edge>(e);
  }

   /**
   * Agrega la arista dada al grafo. Si los vertices de la arista
   * no existen o el grafo tiene una arista entre dichos vertices,
   * retorna false. Si se agrega la nueva arista, retorna true.
   * 
   * Complejidad Propuesta: O(p + q), p << | V | y q << | E |
   * Complenidad Real: O(1), ya que los nodos se agregan al final de las listas,
   * y el orden de add de Hash es O(1).
   */
  public boolean add(Edge e){
    Node nodo1 = new Node(e.getSrc());
    Node nodo2 = new Node(e.getDst());

    if((Nodos.contiene(nodo1,nodo1.toString()))
       &&(Nodos.contiene( nodo2,nodo2.toString())))
         return ((VerticesSrc.agregar(e,e.getSrc()))
                 &&(VerticesDst.agregar(e,e.getDst())));
    return false;
  }
   

   /**
   * Agrega el nodo n. Si el vertice ya existe, retorna false. Si
   * se agrega el nodo, retorna true.
   *
   * Complejidad: O(p), p << | V |
   * Complenidad Real: O(1), ya que los nodos se agregan al final de las listas,
   * y el orden de add de Hash es O(1).
   */
  public boolean add(Node n){
    if(!(Nodos.contiene(n,n.toString()))){
      if(!(Nodos.agregar(n,n.toString()))){
        return false;
      }else{
        return true;
      }
    }else{
        return false;
    }
  }

  /*
  * Elimina los nodos y aristas del grafo.
  * 
  * Complejidad: O(1)
  * Complejidad Real: O(1)
  */
  public void clear(){
    Nodos.borrado_completo();
    VerticesDst.borrado_completo();
    VerticesSrc.borrado_completo();
  }


  /*
  * Chequea que el grafo contiene una arista (src, dst).
  * 
  * Complejidad: O(p), p << | E |
  * Complejidad Real: O(n/k) donde n es la cantidad de elementos y k es la
  * cantidad de contenedores
  */
  public boolean contains(String src, String dst){
    Edge e = new Edge(src, dst);
    if((VerticesSrc.contiene(e,src))&&(VerticesDst.contiene(e,dst))){
      return true;
    }else{
      return false;
    }

  }

  /*
  * Chequea que el grafo contiene un nodo con el id del nod
  *
  * Complejidad: O(p), p << | V |
  * Complejidad Real: O(n/k) donde n es la cantidad de elementos y k es la cantidad de contenedores
  */
  public boolean contains(String nod) {
    Node n = new Node(nod);
    if((Nodos.contiene(n,n.toString()))){
       return true;
    }else{
       return false;
    }
  }

   /*
   * Retorna la arista del grafo que conecta a los vertices
   * src y dst. Si no existe dicha arista, retorna null.
   * 
   *  Complejidad: O(p), p << | E |
   * Complejidad Real: O(n/k) donde n es la cantidad de elementos y k es la cantidad de contenedores
   */
   public Edge get(String src, String dst){
      return VerticesSrc.obtener(new Edge(src,dst),src);
   }

  /*
  * Retorna todas las aristas del grafo
  *
  * Complejidad: O(n)
  * Donde n es la cantidad de aristas
  */
  public List<Edge> getEdges(){
    return VerticesSrc.obtenerNodos();
  }

   /*
   * Retorna el nodo con id nod. Si no existe dicho nodo, 
   * retorna null.
   *
   * Complejidad: O(p), p << |V|
   * Complejidad Real: O(n/k) donde n es la cantidad de elementos y k es la 
   *   cantidad de contenedores
   */
   
   public Node get(String nod){
      return Nodos.obtener(new Node(nod),nod);
   }

   /* 
   * Retorna todos los nodos del grafo.
   *
   * Complejidad: O(n)
   * Donde n es la cantidad de nodos
   */
   public List<Node> getNodes(){
      return Nodos.obtenerNodos();
   }


   /*
   * Retorna el numero de aristas en el grafo.
   *
   * Complejidad: O(1)
   */
   public int getNumEdges() {
      return VerticesDst.obtenerCantidadListaOcupado();
   }


   /*
   * Retorna el numero de vertices en el grafo.
   * 
   * Complejidad: O(1)
   */
   public int getNumVertices(){
      return Nodos.obtenerCantidadListaOcupado();
   }


  /*
   * Retorna la lista de lados que tienen al vertice dado como
   * destino. Si el vertice no existe, retorna null.
   * 
   * Complejidad: O(p), p << | E |
   * Complejidad Real: O(k) donde k es el tamaño de la lista
   */
  public List<Edge> getInEdges(String n){
    Node e = new Node(n);

    Edge arco_a_revisar;
    Object temporal;
    
    
    List<Edge> colisiones_dst;
    ListIterator<Edge> iterador_dst;

    List<Edge> ListaLados = new MyList<Edge>();

    if(Nodos.contiene(e,e.toString())){

      colisiones_dst= VerticesDst.ListaColisiones(n);

      if (!(colisiones_dst.isEmpty())&&(colisiones_dst != null)){

        iterador_dst =  colisiones_dst.iterator();

        while(iterador_dst.hasNext()){
        
          arco_a_revisar = iterador_dst.next();

          if(arco_a_revisar.verificarDst(n)){
            ListaLados.add(arco_a_revisar);
          }
        }
      }
    }
    return ListaLados;
  }


  /*
   * Retorna la lista de lados que tienen al vertice dado como
   * origen. Si el vertice no existe, retorna null.
   * 
   * Complejidad: O(p), p << | E |
   * Complejidad Real: O(k) donde k es el tamaño de la lista
   */
  public List<Edge> getOutEdges(String n){
    Node e = new Node(n);

    Edge arco_a_revisar;

    List<Edge> colisiones_src;
    ListIterator<Edge> iterador_src;

    List<Edge> ListaLados = new MyList<Edge>();

    if(Nodos.contiene(e,e.toString())){

      colisiones_src= VerticesSrc.ListaColisiones(n);

      if (!(colisiones_src.isEmpty())&&(colisiones_src != null)){

        iterador_src =  colisiones_src.iterator();

        while(iterador_src.hasNext()){
        
          arco_a_revisar = iterador_src.next();

          if(arco_a_revisar.verificarSrc(n)){
            ListaLados.add(arco_a_revisar);
          }
        }
      }
    }
    return ListaLados;
  }


   /*
   * Remueve la arista del grafo que conecta a los vertices src y
   * dst. Si el grafo no cambia, retorna false. Si el grafo cambia,
   * retorna true.
   * 
   * Complejidad: O(p), p << | E |
   * Complejidad Real: O(n/k) donde n es la cantidad de elementos y k es la 
   *   cantidad de contenedores
   */
   public boolean remove(String src, String dst){
      Edge e = new Edge(src, dst);
      return VerticesSrc.eliminar(e, src) && VerticesDst.eliminar(e, dst);
   }


   /*
   * Remueve el nodo del grafo con id nod. Si el grafo no cambia,
   * retorna false. Si el grafo cambia, retorna true.
   *
   * Complejidad: O(p), p << | V |
   * Complejidad Real: O(n/k) donde n es la cantidad de elementos y k es la 
   *   cantidad de contenedores
   */

  public boolean remove(String n){

    Node e = new Node(n);
    Edge arco;

    List<Edge> colisiones;
    ListIterator<Edge> iter;


    if(Nodos.eliminar(e,n)){
      colisiones = VerticesSrc.ListaColisiones(n);

      if (!colisiones.isEmpty()){
        iter =  colisiones.iterator();

        while(iter.hasNext()){
          arco = iter.next();

          if(arco.verificarSrc(n)){
            iter.unlink();
          }
        }
      }

      colisiones = VerticesDst.ListaColisiones(n);

      if (!colisiones.isEmpty()){
        iter =  colisiones.iterator();

        while(iter.hasNext()){
          arco = iter.next();

          if(arco.verificarDst(n)){
            iter.unlink();
          }
        }
      }
      return true;
    }
    return false;
  }

  /*
    Complejidad Real: O(k) donde k es la cantidad de contenedores
  */
  public DigraphHash clone(){
    
    DigraphHash Hash  = new DigraphHash();

    Hash.Nodos = Nodos.clone();
    Hash.VerticesDst = VerticesDst.clone();
    Hash.VerticesSrc= VerticesSrc.clone();

    return Hash;
  }

}
