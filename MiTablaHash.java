/**
 * Clase que implementa la interfaz TablaHash, usando listas para manejar
 * las colisiones
 * @author Paul Baptista (10-10056)
 * @author Fabio Castro (10-10056)
 */

public class MiTablaHash<E> implements TablaHash<E>{
   
   /**
    * Modelo de representacion: Tabla de Hash con manejo de Colisiones.
    *
    */
   private List<E>[] Elementos;
   private int TamanoActualLista;
   private int CantidadListaOcupado;


   /**
    * Constructor
    *
    */
   @SuppressWarnings("unchecked")
   public MiTablaHash(int t){
      if ( t==0) t++;
      TamanoActualLista = t;

      Elementos = (MyList<E>[])(new MyList[TamanoActualLista]);

      for (int i=0;i<TamanoActualLista;i++) {
         Elementos[i] = new MyList<E>();
      }
      CantidadListaOcupado = 0;
   }


   /**
    * Función de Hash
    *
    */
   static int funcion(String n, int CantidadDeElementos) {
     return ((n.hashCode() + 1) % CantidadDeElementos);
   }


   /**
    *Agrega un Elemento a la tabla de Hash, usando el identificador como clave.
    */
   public boolean agregar(E objeto, String identificador){
      int codigo_hash = funcion(identificador , TamanoActualLista);
      if(!(Elementos[codigo_hash].contains(objeto))){
         if(!(Elementos[codigo_hash].add(objeto))){
            return false;
         }else{
            CantidadListaOcupado = CantidadListaOcupado + 1;
            return true;
         }
      }else{
          return false;
      }
   }


   /**
    *Elimina un par (clave,valor) de la tabla de Hash
    */
   public boolean eliminar(E objeto, String identificador){
      int codigo_hash = funcion(identificador , TamanoActualLista);
      if(Elementos[codigo_hash].contains(objeto)){
         if(!(Elementos[codigo_hash].remove(objeto))){
            return false;
         }
            CantidadListaOcupado = CantidadListaOcupado - 1;
            return true;
      }else{
          return false;
      }
   }   


   /**
    * Reinicializa la tabla de Hash
    */
   public void borrado_completo(){

      for (int i=0;i<TamanoActualLista;i++) {
         Elementos[i].clear();
      }

      CantidadListaOcupado = 0;
   }


   /**
    * Verifica que la tabla contenga un par (clave,valor)
    */
   public boolean contiene(E objeto, String identificador){
      int codigo_hash = funcion(identificador , TamanoActualLista);
         if(Elementos[codigo_hash].contains(objeto)){
            return true;
         }else{
            return false;
         }
   }

   /**
    * Retorna una Lista la instacia almacenada en la lista igual a E
    */   
   public E obtener(E objeto, String identificador){
      int codigo_hash = funcion(identificador , TamanoActualLista);
      
      return Elementos[codigo_hash].get(objeto);

   }

   /**
    * Retorna una Lista con todos los elementos de la tabla
    */   
   public List<E> obtenerNodos(){
      List<E> n = new MyList<E>();   
      int x;
      for(x=0;x<TamanoActualLista;x++){
       n.Concat(Elementos[x]);
      }
      return n;
   }

   /**
    * Retorna la cantidad de listas que posee la tabla de Hash
    */
   public int obtenerTamanoActualLista() {
      return TamanoActualLista;
   }

   /**
    * Retorna la cantidad de elementos de la lista
    */
   public int obtenerCantidadListaOcupado() {
      return CantidadListaOcupado;
   }

   /**
    * Hace una copia de la tabla de Hash
    */
   public MiTablaHash<E> clone(){

      MiTablaHash<E> Hash_Retorno  = new MiTablaHash<E>(TamanoActualLista);

      Hash_Retorno.CantidadListaOcupado = CantidadListaOcupado;

      int contador = 0;
      while (contador < TamanoActualLista){
         Hash_Retorno.Elementos[contador] = Elementos[contador].clone();
         contador++;
      }

      return Hash_Retorno;
   }

   /**
    * Devuelve la lista de las colisiones de hash con que coinciden con
    * el identificador
    */
   public List<E> ListaColisiones(String identificador){
      int codigo_hash = funcion(identificador , TamanoActualLista);
      return Elementos[codigo_hash];
   }

   public boolean isEmpty(){
    return CantidadListaOcupado == 0;
   }
}