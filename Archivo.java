/**
 * Archivo.java
 * Clase que implementa el manejo de archivos.
 * 
 * Autores: Fabio Castro   10-10132 
 *          Paul Baptista  10-10056
 * Grupo: 09
 * Fecha: 14/12/2014
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

public class Archivo {
  
  // File Reader 
  // (Nombre del archivo de entrada).
  private FileReader reader;

  // Buffered Reader 
  // (Apuntador a la próxima posición después de lo que lleva leído).
  private BufferedReader read;

  // File Writer 
  // (Nombre del archivo de salida).
  private FileWriter writer;

  // Print Writer 
  // (Apuntador a la próxima posición donde escribir).
  private PrintWriter wrote;

  private Node nodoInicial;
  
  private Node nodoFinal;

  private int cant_mundos_leidos;

  private int cant_mundos_totales;
    
  /**
  * Constructor
  * Inicializa los atributos.
  */
  public Archivo() {
    reader = null;  
    read = null;
    writer = null;
    wrote = null;
    cant_mundos_leidos = 0;
    cant_mundos_totales = 0;
  }
  
  public void openFile(String in, String out) throws Exception {
    /*
     * try - catch 
     * Chequea que el archivo de entrada haya sido abierto
     * correctamente.
     * De lo contrario, indica que hubo un error.
     */
    try {

     // Apunta el FileReader al archivo de entrada.
      reader = new FileReader(in);
      
      // Apunta el BufferedReader al salida del archivo de entrada.
      read = new BufferedReader(reader);
      
      // Apunta de FileWriter al archivo de salida.
      writer = new FileWriter(out);
      
      // Apunta el PrintWriter al salida del archivo de salida.
      wrote = new PrintWriter(writer);

      String[] linea = read.readLine().split(" ");

      cant_mundos_totales = Integer.parseInt(linea[0]);
   
    } catch (Exception ioe) {
      System.err.println("Error abriendo el archivo de entrada.");
      System.exit(1);
    }
        
  }
        
  /**
  * readFile
  * Lee el archivo especificado en el stdin.
  * Crea todos los arcos necesarios, segun lo especificado en el archivo.
  * Retorna: Digrafo con sus nodos y arcos
  */
  public DigraphHash readFile() throws Exception {
    
    try{   
      String[] linea = read.readLine().split(" ");
      int largo = Integer.parseInt(linea[0]);
      linea = read.readLine().split(" ");
      int ancho = Integer.parseInt(linea[0]);
      DigraphHash grafo = new DigraphHash(largo*ancho);
      int j,k;
      int nodos = 0;
        /* Se confirma que sea el ultimo valor, el de cierre del archivo */
        if(cant_mundos_leidos==cant_mundos_totales){
           // System.out.println("El archivo no posee mas niveles. Se cerrara");
          return null;
        }else{
            /* For de recorrido vertical */
            for(j = 1; j <= largo; j++){
              linea = read.readLine().split(" ");
              /* For de recorrido horizontal */
              for(k = 1; k <= ancho; k++){
                  nodos = nodos+1;
                  grafo.add(new Node(""+nodos,Integer.parseInt(linea[k-1])));

                  /* Si es el nodo incial se guarda su id */
                  if(nodos==largo*ancho){
                    nodoFinal = new Node(""+nodos,Integer.parseInt(linea[k-1]));
                    grafo.add(new Edge(""+(nodos-1),""+(nodos)));
                    grafo.add(new Edge(""+(nodos-ancho),""+(nodos)));
                    break;
                  }

                  /* Si es el nodo final se guarda su id */
                  if(nodos==1){
                    nodoInicial = new Node(""+nodos,Integer.parseInt(linea[k-1]));
                  }

                  /* Se crean los arcos este-oeste de los nodos */       
                      
                  if(k>1){
                    grafo.add(new Edge(""+(nodos-1),""+(nodos)));
                    grafo.add(new Edge(""+(nodos),""+(nodos-1)));
                  }

                  /* Se crean los arcos norte-sur de los nodos */
                  if(j>1){
                    grafo.add(new Edge(""+(nodos-ancho), ""+(nodos))); 
                    grafo.add(new Edge(""+(nodos),""+(nodos-ancho)));
                  }
                }
              }
          cant_mundos_leidos = cant_mundos_leidos + 1;
          return grafo;
        }
       } catch (Exception ioe) {
        // Atrapa el error generado cuando un archivo ya fue cerrado 
        // System.err.println("Error , archivo cerrado");
        return null;
      }

  }

  public Node getNodoInicial(){
    return nodoInicial; 
  }
  
  public Node getNodoFinal(){
    return nodoFinal; 
  }
  
  public void writeFile(String elemento_a_escribir) {
    /* 
     * Escribe en la posición donde quedó el apuntador, la 
     * respuesta dada más un salto de línea.
     */ 
    wrote.println(elemento_a_escribir);
  }
  
  public void closeFile() throws Exception {

    /*
     * try - catch 
     * Chequea que los archivos de entrada y salida hayan sido
     * cerrados correctamente.
     * De lo contrario, indica que hubo un error.
     */
    try {

      read.close();
      wrote.close();


    } catch (Exception ioe) {
      // System.err.println("Error cerrando un archivo.");
    }

  }
   
}
