/**
 * Node.java
 * Clase que implementa el manejo de archivos.
 * 
 * Autores: Fabio Castro   10-10132 
 *          Paul Baptista  10-10056
 * Grupo: 09
 * Fecha: 15/12/2013
 */

public class Node implements Comparable<Node>{

   private int peso = 0;
   protected String id = null;

   public Node(String i){
      id = new String(i);
   }

   public Node(String i, int pe){
      id = new String(i);
      peso = pe;
   }

   public int getPeso(){
      return peso;
   }


   public int compareTo(Node other){           //es necesario definir un comparador para el correcto funcionamiento del PriorityQueue
      if(other == null){ System.out.println("Error, nodo nulo");};
      if( peso > other.peso ){ return 1; };
      if( peso == other.peso ){ return 0; };
      return -1;
   }

   /**
   * Retorna una nueva arista que es copia de this.
   */
   @Override
   protected Object clone() {
   return new Node(id);
   }

   /**
   * Indica si la arista de entrada es igual a this.
   */
   public boolean equals(Object o) {

   Node d;

   if (!(o instanceof Node))
      return false;

   d = (Node) o;

      return d.id.equalsIgnoreCase(this.id);
   }

   /**
   * Retorna la representacion en String de la arista.
   */
   @Override
   public String toString() {
      return new String(id);
   }


}